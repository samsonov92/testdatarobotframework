*** Test Cases ***
Test suit
    [Tags]    DELETE
    Check_Patch_Request


*** Settings ***
Resource          setup.robot

*** Keywords ***
Check_Patch_Request
     Create Session  test_session  ${ROOT URL}
     ${response}=  Delete Request  test_session  /users/admin  headers=${headers json}
     Should Be Equal As Strings  ${response.status_code}  405

     Create Session  session_test_data  ${ROOT URL}  headers=${headers json}
     ${response}=  Get Request  session_test_data  /users
     Should Be Equal As Strings  ${response.status_code}  200
     Should Be True      """admin""" not in """${response.text}"""