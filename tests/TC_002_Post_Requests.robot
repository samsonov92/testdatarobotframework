*** Test Cases ***
Test suit
    [Tags]    POST
    Check_Post_Request


*** Settings ***
Resource          setup.robot

*** Keywords ***
Check_Post_Request
     Create Session  test_session  ${ROOT URL}
     ${response}=  Post Request  test_session  /users  data={"username": "Name1", "password": "Password1", "description": "Description1"}  headers=${headers json}
     Should Be Equal As Strings  ${response.status_code}  201

     Create Session  session_test_data  ${ROOT URL}  headers=${headers json}
     ${response}=  Get Request  session_test_data  /users
     Should Be Equal As Strings  ${response.status_code}  200
     Should Be True      """${test user name}""" in """${response.text}"""

