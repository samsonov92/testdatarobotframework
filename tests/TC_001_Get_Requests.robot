*** Test Cases ***
Test suit
    [Tags]    GET
    Check_Get_Requests


*** Settings ***
Resource          setup.robot

*** Keywords ***
Check_Get_Requests
     Create Session  session_test_data  ${ROOT URL}
     ${response}=  Get Request  session_test_data  /
     Should Be Equal As Strings  ${response.status_code}  403
     Should Be True      "Forbidden" in """${response.text}"""

     Create Session  session_test_data  ${ROOT URL}  headers=${headers json}
     ${response}=  Get Request  session_test_data  /
     Should Be Equal As Strings  ${response.status_code}  200
     Should Be True      "Доступные endpoints:" in """${response.text}"""

