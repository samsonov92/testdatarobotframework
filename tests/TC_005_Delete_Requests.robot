*** Test Cases ***
Test suit
    [Tags]    DELETE
    Check_Patch_Request


*** Settings ***
Resource          setup.robot

*** Keywords ***
Check_Patch_Request
     Create Session  test_session  ${ROOT URL}
     ${response}=  Delete Request  test_session  /users/${test user name}  headers=${headers json}
     Should Be Equal As Strings  ${response.status_code}  204

     Create Session  session_test_data  ${ROOT URL}  headers=${headers json}
     ${response}=  Get Request  session_test_data  /users
     Should Be Equal As Strings  ${response.status_code}  200
     Should Be True      """${test user name}""" not in """${response.text}"""