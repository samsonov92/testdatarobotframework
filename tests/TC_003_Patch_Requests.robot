*** Test Cases ***
Test suit
    [Tags]    PATCH
    Check_Patch_Request


*** Settings ***
Resource          setup.robot

*** Keywords ***
Check_Patch_Request
     Create Session  test_session  ${ROOT URL}
     ${response}=  Patch Request  test_session  /users  data={"username": "Name1", "password": "PASSWORD", "description": "check_patch_request"}  headers=${headers json}
     Should Be Equal As Strings  ${response.status_code}  200

     Create Session  session_test_data  ${ROOT URL}  headers=${headers json}
     ${response}=  Get Request  session_test_data  /users
     Should Be Equal As Strings  ${response.status_code}  200
     Should Be True      """${test user name}""" in """${response.text}"""